#!/bin/sh
DRUPAL_VER=7.42
DRUPAL_SRC=https://ftp.drupal.org/files/projects/drupal-$DRUPAL_VER.tar.gz
DBPASSWD=Vagrant123

echo "Updating and upgrading"
apt-get update
#apt-get upgrade -y


if [ ! -e "/etc/apache2" ]
then
    echo "Installing LAMP"
#    apt-get install -y apache2

echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections 
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections 

#sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password rootpass'
#sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password rootpass'

sudo apt-get install -y lamp-server^
    #a2enmod rewrite
#    cp /vagrant/provision/apache2-envvars /etc/apache2/envvars
    #sudo chown vagrant /var/lock/apache2
    #usermod -a -G adm vagrant
fi

# download drupal
if [ ! -e "/var/www/index.php" ]
then
    echo "Downloading Drupal"
    #mkdir /vagrant/web/
    cd /var/www
    wget --quiet $DRUPAL_SRC
    tar --strip-components=1 -xzf `basename $DRUPAL_SRC`
    rm `basename drupal-$DRUPAL_VER.tar.gz`
    #mv drupal-$DRUPAL_VER/* drupal-$DRUPAL_VER/.htaccess ./
    #mv drupal-$DRUPAL_VER/.gitignore ./
#    rm /vagrant/web/sites/default/default.settings.php
fi

cd /var/www
rm index.html
apt-get install php5-gd -y
#cp sites/default/default.settings.php sites/default/settings.php
cp /vagrant/settings.php sites/default/settings.php
mkdir sites/default/files
chmod 777 sites/default/files
chmod 666 sites/default/settings.php

service apache2 restart

mysql --user=root --password=Vagrant123 -e "CREATE DATABASE drupal CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysql --user=root --password=Vagrant123 -e "CREATE USER drupaldbuser@localhost IDENTIFIED BY 'VagrantDBUser123';"
mysql --user=root --password=Vagrant123 -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON drupal.* TO 'drupaldbuser'@'localhost' IDENTIFIED BY 'VagrantDBUser123';"

echo "site should now be available at localhost:8081/install.php"